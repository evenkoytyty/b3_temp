from rest_framework import permissions
from django.contrib.auth.mixins import AccessMixin
from django.db.models import Q

from access_management_django.rules.models import Rule
from django.http import HttpResponse, JsonResponse


class UnauthException(Exception):
    pass


def return_exeption(text="Недостаточно прав", code=403):
    return JsonResponse(
        {
            'error_fields': None,
            'error_description': text,
        }
        , status=code)


class BlocklistPermission(permissions.BasePermission):
    """

    """

    def filter_query(self, request, rules, view):
        q_objects = Q()

        can_get_all = False
        for r in rules:
            regulation = r.regulation
            regulation_result = {}

            if not regulation:
                can_get_all = True
                break
            try:
                for reg in regulation:
                    val = request.user
                    for col in regulation[reg].split('__'):
                        val = getattr(val, col, None)
                    if getattr(val, 'model', None):
                        regulation_result[reg + '__in'] = val.all()
                    else:
                        if val:
                            regulation_result[reg] = val
                        else:
                            pass
            except Exception as e:
                return return_exeption(text="Внутренняя ошибка сервера", code=500)

            q_objects |= Q(**regulation_result)

        if not can_get_all:
            if not view.queryset:
                view.queryset = view.get_queryset()
            view.queryset = view.queryset.filter(q_objects)

    def has_permission(self, request, view):
        try:
            if not request.user.is_superuser:
                rules = Rule.objects.all().filter(role__in=request.user.roles.all(),
                                                  action=request.method.lower(),
                                                  table=view.basename)
                if not rules.count():
                    return False
                self.filter_query(request, rules, view)

            return True

        except UnauthException as e:
            return False
        except Exception as e:
            return False

