import json

from django.apps import apps
from django.test import TestCase
from django.test.client import RequestFactory

from django.test import Client


class TokenTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_token(self):
        c = Client()
        response = c.post('/api/token/', {})
        self.assertEqual(response.status_code, 200)

    def test_get_auth(self):
        c = Client()
        token = c.post('/api/token/', {})
        token = json.loads(token.content).get('token', None)
        auth_headers = {
            'HTTP_AUTHORIZATION': f'bearer {token}',
        }
        response = c.get('/api/auth/', **auth_headers)
        self.assertEqual(response.status_code, 200)


class EsiaAuthTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_without_login_code(self):
        c = Client()
        token = c.post('/api/token/', {})
        token = json.loads(token.content).get('token', None)
        auth_headers = {
            'HTTP_AUTHORIZATION': f'bearer {token}',
        }
        response = c.get('/esia/login/', **auth_headers)
        url = getattr(response, 'url', '')
        self.assertEqual(url.find('error') >= 0 and url.find('error_description') >= 0, True)

    def test_login_code_not_valid(self):
        c = Client()
        Token = apps.get_model('model_app', 'Token')
        token = c.post('/api/token/', {})
        token = json.loads(token.content).get('token', None)
        try:
            token = Token.objects.get(token=token)
            auth_headers = {
                'HTTP_AUTHORIZATION': f'bearer {token.token}',
            }
            url_follow = '/esia/login/?login_code=123'
            response = c.get(url_follow, **auth_headers)
            url = getattr(response, 'url', '')
            self.assertEqual(url.find('error') >= 0 and url.find('error_description') >= 0, True)
        except Token.DoesNotExist:
            self.assertEqual(False, True)

    def test_login_code(self):
        c = Client()
        Token = apps.get_model('model_app', 'Token')
        token = c.post('/api/token/', {})
        token = json.loads(token.content).get('token', None)
        try:
            token = Token.objects.get(token=token)
            auth_headers = {
                'HTTP_AUTHORIZATION': f'bearer {token.token}',
            }
            response = c.get('/api/auth/', **auth_headers)
            url_follow = json.loads(response.content).get('url_follow')
            response = c.get(url_follow, **auth_headers)
            url = getattr(response, 'url', '')
            self.assertEqual(url.find('error') < 0 and url.find('error_description') < 0, True)
        except Token.DoesNotExist:
            self.assertEqual(False, True)


class EsiaAuthOrgTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_orgs(self):
        c = Client()
        Token = apps.get_model('model_app', 'Token')
        token = c.post('/api/token/', {})
        token = json.loads(token.content).get('token', None)
        try:
            token = Token.objects.get(token=token)
            token.esia_auth_data = {
                "code": "eyJ2ZXIiOjEsInR5cCI6IkpXVCIsInNidCI6ImF1dGhvcml6YXRpb25fY29kZSIsImFsZyI6IlJTMjU2In0.eyJuYmYiOjE2MDYxMTc2ODAsInNjb3BlIjoidXNyX29yZz9vaWQ9MTAxNTE4NDM5OSBlbWFpbD9vaWQ9MTAxNTE4NDM5OSBvcGVuaWQgZnVsbG5hbWU_b2lkPTEwMTUxODQzOTkiLCJhdXRoX3RpbWUiOjE2MDYxMTc2ODAsImlzcyI6Imh0dHA6XC9cL2VzaWEuZ29zdXNsdWdpLnJ1XC8iLCJ1cm46ZXNpYTpzaWQiOiJmNjcwNDljOC03ZGIwLTRmOWEtYmU5MC03NTlkOGQ2ZDlkNzIiLCJ1cm46ZXNpYTpjbGllbnQ6c3RhdGUiOiIzMzY2MTRhYS0yZDYwLTExZWItYWNkZi04MjM5OTcxM2Q0MTEiLCJhdXRoX210aGQiOiJQV0QiLCJ1cm46ZXNpYTpzYmoiOnsidXJuOmVzaWE6c2JqOnR5cCI6IlAiLCJ1cm46ZXNpYTpzYmo6aXNfdHJ1Ijp0cnVlLCJ1cm46ZXNpYTpzYmo6b2lkIjoxMDE1MTg0Mzk5LCJ1cm46ZXNpYTpzYmo6bmFtIjoiT0lELjEwMTUxODQzOTkiLCJ1cm46ZXNpYTpzYmo6ZWlkIjoyNDM4MDU3Mn0sImV4cCI6MTYwNjExNzkyMCwicGFyYW1zIjp7InJlbW90ZV9pcCI6Ijk1LjI0LjI2Ljg5IiwidXNlcl9hZ2VudCI6Ik1vemlsbGFcLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0XC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWVcLzg3LjAuNDI4MC42NiBTYWZhcmlcLzUzNy4zNiJ9LCJpYXQiOjE2MDYxMTc2ODAsImNsaWVudF9pZCI6IlJQUk4wMDEwMSJ9.bq-VwNQnyemKLkTBMZoN9HWqIuYvp78wp_R1nMZJdYyfYkiub0CDq1S-EyrmiGWRskqUQrVR1t0hLzITevQ7lK6rduGzdYiwX_6VH29S7eonud8OS_7oONfgWy9z3c1Bn9iCDjqRHDv0kPSbELDs6Nhqq50glrNk-YZOpaajG-QFNoiBLRE4Fcvre_89dZdX3GCtSxgJjrRi7urLU9o_ROZeNQlgIY4YQgMo1hWQ3qHM2i2g8sbev8jRHOrufK2wSzy32weVNJDjKGxmZKmIafLBVKKkHZ0TTlGyb_WnuCSIudXk7aNGZdQ3nTr4i5dD6ziFKt33Kkq5SP6Osejqfw",
                "orgs": [
                    {"oid": 1000334489, "name": "ФГБУ \"ФЦАО\"", "ogrn": "1037739128129", "exist": True},
                    {"oid": 1021099110, "name": "ООО \"БОЛЬШАЯ ТРОЙКА\"", "ogrn": "1107746574308", "exist": True}],
                "email": "test@yandex.ru",
                "state": "336614aa-2d60-11eb-acdf-82399713d411",
                "user_oid": "123",
                "companies": [{"oid": 1000334489, "name": "ФГБУ \"ФЦАО\"", "ogrn": "1037739128129", "exist": True},
                              {"oid": 1021099110, "name": "ООО \"БОЛЬШАЯ ТРОЙКА\"", "ogrn": "1107746574308",
                               "exist": True}],
                "last_name": "Test",
                "first_name": "Test",
                "domain_debug": "http://app.dev.oc.big3.ru"}
            token.save()
            auth_headers = {
                'HTTP_AUTHORIZATION': f'bearer {token}',
            }
            response = c.get('/api/auth/', **auth_headers)
            content = json.loads(response.content)
            companies = content.get('companies', None)
            if companies:
                company = companies[0]
                response = c.get(company.get('url_follow', None), **auth_headers)
                url = getattr(response, 'url', '')
                self.assertEqual(url.find('error') < 0 and url.find('error_description') < 0, True)
            else:
                self.assertEqual(False, True)
        except Token.DoesNotExist:
            self.assertEqual(False, True)

    def test_orgs_without_login_code(self):
        c = Client()
        Token = apps.get_model('model_app', 'Token')
        token = c.post('/api/token/', {})
        token = json.loads(token.content).get('token', None)
        try:
            token = Token.objects.get(token=token)
            token.login_code = '333'
            token.save()
            auth_headers = {
                'HTTP_AUTHORIZATION': f'bearer {token}',
            }
            response = c.get('/esia/org/123/', **auth_headers)
            url = getattr(response, 'url', '')
            self.assertEqual(url.find('error') >= 0 and url.find('error_description') >= 0, True)
        except Token.DoesNotExist:
            self.assertEqual(False, True)

    def test_orgs_without_esia_data(self):
        c = Client()
        Token = apps.get_model('model_app', 'Token')
        token = c.post('/api/token/', {})
        token = json.loads(token.content).get('token', None)
        try:
            token = Token.objects.get(token=token)
            token.login_code = '123'
            token.save()
            auth_headers = {
                'HTTP_AUTHORIZATION': f'bearer {token}',
            }
            response = c.get('/esia/org/111/?login_code=123', **auth_headers)
            url = getattr(response, 'url', '')
            self.assertEqual(url.find('error') >= 0 and url.find('error_description') >= 0, True)
        except Token.DoesNotExist:
            self.assertEqual(False, True)


class EsiaAuthResponseTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_not_params(self):
        c = Client()
        response = c.get('/esia/response/')
        url = getattr(response, 'url', '')
        self.assertEqual(url.find('error') >= 0 and url.find('error_description') >= 0, True)

    def test_with_error(self):
        c = Client()
        response = c.get('/esia/response/?error=500')
        url = getattr(response, 'url', '')
        self.assertEqual(url.find('error') >= 0 and url.find('error_description') >= 0, True)

    def test_with_state(self):
        c = Client()
        response = c.get('/esia/response/?state=123')
        url = getattr(response, 'url', '')
        self.assertEqual(url.find('error') >= 0 and url.find('error_description') >= 0, True)

    def test_response(self):
        error = ''
        c = Client()
        Token = apps.get_model('model_app', 'Token')
        token = c.post('/api/token/', {})
        token = json.loads(token.content).get('token', None)
        try:
            token = Token.objects.get(token=token)
            token.esia_auth_data = {
                "code": "eyJ2ZXIiOjEsInR5cCI6IkpXVCIsInNidCI6ImF1dGhvcml6YXRpb25fY29kZSIsImFsZyI6IlJTMjU2In0.eyJuYmYiOjE2MDYxMTc2ODAsInNjb3BlIjoidXNyX29yZz9vaWQ9MTAxNTE4NDM5OSBlbWFpbD9vaWQ9MTAxNTE4NDM5OSBvcGVuaWQgZnVsbG5hbWU_b2lkPTEwMTUxODQzOTkiLCJhdXRoX3RpbWUiOjE2MDYxMTc2ODAsImlzcyI6Imh0dHA6XC9cL2VzaWEuZ29zdXNsdWdpLnJ1XC8iLCJ1cm46ZXNpYTpzaWQiOiJmNjcwNDljOC03ZGIwLTRmOWEtYmU5MC03NTlkOGQ2ZDlkNzIiLCJ1cm46ZXNpYTpjbGllbnQ6c3RhdGUiOiIzMzY2MTRhYS0yZDYwLTExZWItYWNkZi04MjM5OTcxM2Q0MTEiLCJhdXRoX210aGQiOiJQV0QiLCJ1cm46ZXNpYTpzYmoiOnsidXJuOmVzaWE6c2JqOnR5cCI6IlAiLCJ1cm46ZXNpYTpzYmo6aXNfdHJ1Ijp0cnVlLCJ1cm46ZXNpYTpzYmo6b2lkIjoxMDE1MTg0Mzk5LCJ1cm46ZXNpYTpzYmo6bmFtIjoiT0lELjEwMTUxODQzOTkiLCJ1cm46ZXNpYTpzYmo6ZWlkIjoyNDM4MDU3Mn0sImV4cCI6MTYwNjExNzkyMCwicGFyYW1zIjp7InJlbW90ZV9pcCI6Ijk1LjI0LjI2Ljg5IiwidXNlcl9hZ2VudCI6Ik1vemlsbGFcLzUuMCAoWDExOyBMaW51eCB4ODZfNjQpIEFwcGxlV2ViS2l0XC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWVcLzg3LjAuNDI4MC42NiBTYWZhcmlcLzUzNy4zNiJ9LCJpYXQiOjE2MDYxMTc2ODAsImNsaWVudF9pZCI6IlJQUk4wMDEwMSJ9.bq-VwNQnyemKLkTBMZoN9HWqIuYvp78wp_R1nMZJdYyfYkiub0CDq1S-EyrmiGWRskqUQrVR1t0hLzITevQ7lK6rduGzdYiwX_6VH29S7eonud8OS_7oONfgWy9z3c1Bn9iCDjqRHDv0kPSbELDs6Nhqq50glrNk-YZOpaajG-QFNoiBLRE4Fcvre_89dZdX3GCtSxgJjrRi7urLU9o_ROZeNQlgIY4YQgMo1hWQ3qHM2i2g8sbev8jRHOrufK2wSzy32weVNJDjKGxmZKmIafLBVKKkHZ0TTlGyb_WnuCSIudXk7aNGZdQ3nTr4i5dD6ziFKt33Kkq5SP6Osejqfw",
                "orgs": [
                    {"oid": 1000334489, "name": "ФГБУ \"ФЦАО\"", "ogrn": "1037739128129", "exist": True},
                    {"oid": 1021099110, "name": "ООО \"БОЛЬШАЯ ТРОЙКА\"", "ogrn": "1107746574308", "exist": True}],
                "email": "test@yandex.ru",
                "state": "336614aa-2d60-11eb-acdf-82399713d411",
                "user_oid": "123",
                "companies": [{"oid": 1000334489, "name": "ФГБУ \"ФЦАО\"", "ogrn": "1037739128129", "exist": True},
                              {"oid": 1021099110, "name": "ООО \"БОЛЬШАЯ ТРОЙКА\"", "ogrn": "1107746574308",
                               "exist": True}],
                "last_name": "Test",
                "first_name": "Test",
                "domain_debug": "http://app.dev.oc.big3.ru"}
            token.save()
            response = c.get(
                f'/esia/response/?state={token.esia_auth_data["state"]}&code={token.esia_auth_data["code"]}')
            url = getattr(response, 'url', '')
            if url.find('error') >= 0 and url.find('error_description') >= 0:
                if url.find('error=400') >= 0:
                    error = 400
            self.assertEqual(error, 400)
        except Token.DoesNotExist:
            self.assertEqual(False, True)
